Værsågod Bootstrap Script
=========================

Local (Valet/MAMP)
------------------
Install will assume that you're running database on localhost, with a 
local version of PHP and composer installed.  

To bootstrap using the latest version of Craft 4.x:  

```
bash <(curl -s https://bitbucket.org/vaersaagod/vaersagod-bootstrap-2018-craft-3/raw/master/go.sh)
```

To bootstrap using the latest version of Craft 3.x:  

```
bash <(curl -s https://bitbucket.org/vaersaagod/vaersagod-bootstrap-2018-craft-3/raw/master/go.craft3.sh)
```

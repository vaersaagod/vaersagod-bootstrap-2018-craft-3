#!/bin/bash

function coloredEcho {
    local exp=$1;
    local color=$2;
    local mod=$3;
    if ! [[ $color =~ '^[0-9]$' ]] ; then
       case $(echo $color | tr '[:upper:]' '[:lower:]') in
        black) color=0 ;;
        red) color=1 ;;
        green) color=2 ;;
        yellow) color=3 ;;
        blue) color=4 ;;
        magenta) color=5 ;;
        cyan) color=6 ;;
        white|*) color=7 ;; # white or invalid color
       esac
    fi
    tput setaf $color;
    echo $mod "$exp";
    tput sgr0;
}

function program_is_installed {
  local val=1
  type $1 >/dev/null 2>&1 || { local val=0; }
  echo "$val"
}

####### DETECT NEEDED SOFTWARE #######
SOFTWARE_OK=1

if [[ -f ~/.nvm/nvm.sh ]]; then
  . ~/.nvm/nvm.sh
else
  coloredEcho "✗ This script needs nvm installed on your system." red
  coloredEcho "See https://github.com/creationix/nvm#installation" red
  echo ''
  SOFTWARE_OK=0
fi

if [[ $(program_is_installed node) == 1 ]]; then
  NODE=$(which node)
else
  coloredEcho "✗ This script needs node installed on your system." red
  coloredEcho "See https://nodejs.org/en/download/" red
  echo ''
  SOFTWARE_OK=0
fi

if [[ $(program_is_installed mysql) == 1 ]]; then
  MYSQL=$(which mysql)
else
  coloredEcho "✗ You need MySQL installed on your system." red
  coloredEcho "See https://dev.mysql.com/downloads/mysql/" red
  echo ''
  SOFTWARE_OK=0
fi

if [[ $(program_is_installed yarn) == 1 ]]; then
  YARN=$(which yarn)
else
  coloredEcho "✗ You need Yarn installed on your system." red
  coloredEcho "See https://yarnpkg.com/en/docs/install" red
  echo ''
  SOFTWARE_OK=0
fi

if [[ $(program_is_installed composer) == 1 ]]; then
  COMPOSER=$(which composer)
else
  coloredEcho "✗ You need Composer installed on your system." red
  coloredEcho "See https://getcomposer.org/download/" red
  echo ''
  SOFTWARE_OK=0
fi

if [[ $SOFTWARE_OK == 0 ]]; then
  exit
fi

####### USER INPUT #######
echo "";
coloredEcho "Project directory (or ENTER for current): " cyan -n
read projectDir

####### HANDLE DIRECTORY INPUT #######
if [[ -z $projectDir ]]; then
  coloredEcho "Using current directory..." green
  echo ''
else
  projectDirSafe=${projectDir//[^a-zA-Z0-9.]/}
  if [[ -d "$projectDirSafe" ]]; then
    coloredEcho "✗ The directory '$projectDirSafe' already exits. Overwrite it? [y/N]: " red -n
    read projectDirOverwrite
    if [[ $projectDirOverwrite =~ ^[Yy]$ ]]; then
      rm -rf $projectDirSafe
    else
      coloredEcho "✗ Okay then, you figure it out..." red
      exit
    fi
  fi
  coloredEcho "Creating directory '$projectDirSafe' and switching to it..." green
  mkdir -- "$projectDirSafe" && cd -- "$projectDirSafe"
  echo ''
fi

coloredEcho "Project handle (or ENTER to use directory name): " cyan -n
read projectName
if [[ -z $projectName ]]; then
  projectName="${PWD##*/}"
fi

coloredEcho "Using project handle '$projectName'... " green -n
echo ''

coloredEcho "Project description (will be used for package.json): " cyan -n
read projectDesc

coloredEcho "Run default content migrations? [y/N]: " cyan -n
read runMigrations

coloredEcho "Download and use the latest LTS of Node? [y/N]: " cyan -n
read updateNode

####### CREATE DATABASE #######
echo ''
export MYSQL_PWD=root
coloredEcho "Creating database craftcms_$projectName..." green
$MYSQL -u root -e "CREATE DATABASE IF NOT EXISTS craftcms_$projectName";
coloredEcho "✔ DONE" white

####### DOWNLOAD CRAFT #######
echo ''
coloredEcho 'Downloading Craft with Composer...' green

composer create-project -s RC craftcms/craft:^4 . --no-interaction

# Save the security key Craft generated for us
source .env
rm -f .env
rm -f .env.example
rm -rf config
rm -rf templates
rm -rf web
rm -rf modules

# Config composer.json
coloredEcho 'Configuring composer.json...' green
composer config minimum-stability dev
composer config prefer-stable true
composer config preferred-install dist
composer config platform.php "8.0.2"
composer update

echo ''

####### INSTALL EXTRA COMPOSER DEPENDENCIES #######
coloredEcho "Installing additional Composer dependencies..." green

# Craft plugins
declare -a pluginPackages
declare -a pluginHandles

pluginPackages[0]="vaersaagod/toolmate:^1.0"
pluginPackages[1]="vaersaagod/seomate:^2.0"
pluginPackages[2]="craftcms/ckeditor:^3.0"
pluginPackages[3]="spacecatninja/imager-x:^4.0"
pluginPackages[4]="mmikkel/child-me:^1.0"
pluginPackages[5]="mmikkel/cp-field-inspect:^1.0"
pluginPackages[6]="mmikkel/cp-clearcache:^1.0"
pluginPackages[7]="vaersaagod/assetmate:^2.0"
pluginPackages[8]="vaersaagod/redirectmate:^1.0-beta"

pluginHandles[0]="toolmate"
pluginHandles[1]="seomate"
pluginHandles[2]="ckeditor"
pluginHandles[3]="imager-x"
pluginHandles[4]="child-me"
pluginHandles[5]="cp-field-inspect"
pluginHandles[6]="cp-clearcache"
pluginHandles[7]="assetmate"
pluginHandles[8]="redirectmate"

composerInstallStr="";

for i in "${pluginPackages[@]}"
do
    composerInstallStr="$composerInstallStr $i";
done

# Add redis extension
composerInstallStr="$composerInstallStr yiisoft/yii2-redis";

composer require $composerInstallStr --prefer-dist -W;

echo ''
coloredEcho "✔ Done with all the Composer stuff" white

####### DOWNLOAD VÆRSÅGOD BOILERPLATE #######
echo ''
mkdir -p tmp
coloredEcho 'Downloading the Værsågod boilerplate...' pink
git clone --depth=1 --quiet git@bitbucket.org:vaersaagod/vaersagod-boilerplate-2018-craft-3.git tmp
rm -rf tmp/.git

sed -i~ "/^PRIMARY_SITE_URL=/s/=.*/=http:\/\/${projectName}.test/" ./tmp/.env-example;
sed -i~ "/^CRAFT_SECURITY_KEY=/s/=.*/=${CRAFT_SECURITY_KEY}/" ./tmp/.env-example;
sed -i~ "/^CRAFT_APP_ID=/s/=.*/=${CRAFT_APP_ID}/" ./tmp/.env-example;
sed -i~ "/^CRAFT_DB_SERVER=/s/=.*/=localhost/" ./tmp/.env-example;
sed -i~ "/^CRAFT_DB_USER=/s/=.*/=root/" ./tmp/.env-example;
sed -i~ "/^CRAFT_DB_PASSWORD=/s/=.*/=root/" ./tmp/.env-example;
sed -i~ "/^CRAFT_DB_DATABASE=/s/=.*/=craftcms_${projectName}/" ./tmp/.env-example;
sed -i~ "/^CRAFT_DB_TABLE_PREFIX=/s/=.*/=craft/" ./tmp/.env-example;
sed -i~ "/^CRAFT_DB_PORT=/s/=.*/=3306/" ./tmp/.env-example;

echo "" >> ./tmp/.env-example;
echo "HI_MY_NAME_IS=${persona}" >> ./tmp/.env-example;

find ./tmp -type f \( -name "*.php" -o -name "*.json" -o -name "*.conf" -o -name "*.md" -o -name "*.js" -o -name "*.txt" -o -name "*.webmanifest" -o -name ".env-example" \) -exec sed -i '' "s/VRSG_PROJECT_NAME/${projectName}/g" {} \;
find ./tmp -type f \( -name "*.php" -o -name "*.json" -o -name "*.conf" -o -name "*.md" -o -name "*.js" -o -name "*.txt" -o -name "*.webmanifest" -o -name ".env-example" \) -exec sed -i '' "s/VRSG_PROJECT_DESC/${projectDesc}/g" {} \;

cp -r ./tmp/. ./
rm -rf tmp

# Rename the path to the module
mv ./modules/VRSG_PROJECT_NAME "./modules/${projectName}"

# Lock the Yarn version
yarn set version 1

coloredEcho "✔ DONE" white
echo ''

####### NVM/NODE SETUP #######
if [[ $updateNode =~ ^[Yy]$ ]]; then
  echo ''
  lts=$(nvm version-remote --lts)
  coloredEcho "Downloading and installing the latest LTS of Node: $lts..." green
  echo $lts > .nvmrc
  nvm install
  nvm use

  coloredEcho "✔ DONE" white
  echo ''
else
  echo $(nvm current) > .nvmrc
fi

nvm use

####### CLEANING UP #######
mv .env-example .env
rm -f .env-example

####### SETTING PERMISSIONS #######
echo ''
coloredEcho 'Setting permission levels...' green
permLevel=774
chmod $permLevel config
chmod $permLevel storage
chmod $permLevel vendor
coloredEcho "✔ DONE" white
echo ''

####### CREATE VALET/HERD VHOST IF AVAILABLE #######
valetCommand="valet"
VALET=""

if [[ $(program_is_installed herd) == 1 ]]; then
  coloredEcho "You have Herd installed, so I assume that's what you want to use!" cyan
  valetCommand="herd"
  VALET=$(which herd)
elif [[ $(program_is_installed valet) == 1 ]]; then
  VALET=$(which valet)
else
  coloredEcho "Valet install not found!" red
fi

if [[ $VALET != '' ]]; then

  coloredEcho "Do you want to create a vhost for $projectName.test? [y/N]: " cyan -n
  read createValetHost

  if [[ $createValetHost =~ ^[Yy]$ ]]; then

      coloredEcho "Setting up host $projectName.test..." green;
      coloredEcho "Do you want to secure the $projectName.test host? [y/N]: " cyan -n
      read createSecureValetHost

      if [[ $createSecureValetHost =~ ^[Yy]$ ]]; then
          $VALET link $projectName && $VALET secure $projectName;
          sed -i~ "/^PRIMARY_SITE_URL=/s/=.*/=https:\/\/${projectName}.test/" ./.env;
          echo '';
          coloredEcho "Secure host https://$projectName.test created. To remove it, type $ ${valetCommand} unlink. To unsecure the host, type $ ${valetCommand} unsecure" green
      else
          $VALET link $projectName;
          echo '';
          coloredEcho "Host http://$projectName.test created. To remove it, type $ ${valetCommand} unlink" green
      fi

    fi

fi

####### INSTALLING CRAFT ######
coloredEcho "Now for the exciting part. Let's install Craft!" green
echo ''
./$projectFolder/craft install

coloredEcho "✔ DONE" white
echo ''

###### INSTALL PLUGINS (CRAFT) ######
for i in "${pluginHandles[@]}"
do
    ./craft plugin/install $i
done

echo ''
coloredEcho "✔ Plugins Craft install done" white

if [[ $runMigrations =~ ^[Yy]$ ]]; then
    coloredEcho 'Running default migrations...' green
    ./craft migrate/up --interactive=0
    coloredEcho "✔ DONE" white
else
    rm ./migrations/m200324_104915_add_defaults.php
fi

#### CK EDITOR CONFIGS
coloredEcho "Copying CKEditor configs..." cyan
rm -rf ./config/project/ckeditor/configs
mv ./_ckeditor ./config/project/ckeditor/configs
./craft project-config/touch
./craft project-config/apply

coloredEcho "✔ DONE" white
echo ''

echo ''
coloredEcho "Do you want to install front end dependencies with Yarn now? [y/N]: " cyan -n
read installYarnDependencies

if [[ $installYarnDependencies =~ ^[Yy]$ ]]; then
    coloredEcho 'Installing front end dependencies...' green
    yarn && yarn build
fi

coloredEcho "✔ DONE" white
echo ''

echo ''
coloredEcho "Now..." green
echo '';

if [[ $createValetHost =~ ^[Yy]$ ]]; then
    coloredEcho " * Check settings in the .env file" magenta
else
    coloredEcho " * Create a host for http://$projectName.test" magenta
    coloredEcho " * Check settings in the .env file" magenta
fi

if [[ $installYarnDependencies =~ ^[Nn]$ ]]; then
    coloredEcho " * Type $ yarn && yarn build" magenta
fi

coloredEcho " * Type $ yarn start" magenta

echo ''

coloredEcho "That's it. Now make something awesome!" white
echo ''

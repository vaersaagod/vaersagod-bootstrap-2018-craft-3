#!/bin/bash

function coloredEcho {
    local exp=$1;
    local color=$2;
    local mod=$3;
    if ! [[ $color =~ '^[0-9]$' ]] ; then
       case $(echo $color | tr '[:upper:]' '[:lower:]') in
        black) color=0 ;;
        red) color=1 ;;
        green) color=2 ;;
        yellow) color=3 ;;
        blue) color=4 ;;
        magenta) color=5 ;;
        cyan) color=6 ;;
        white|*) color=7 ;; # white or invalid color
       esac
    fi
    tput setaf $color;
    echo $mod "$exp";
    tput sgr0;
}

echo "";
coloredEcho "Choose your fighter:" cyan

personas=("Andre" "Alexander" "Mats")

select persona in "${personas[@]}"; do
  case $persona in
    "Andre")
      coloredEcho "You chose ANDRE" magenta
      break
      ;;
    "Alexander")
      coloredEcho "You chose ALEXANDER" magenta
      break
      ;;
    "Mats")
      coloredEcho "You chose MATS" magenta
      break
      ;;
    *) coloredEcho "You don't work here." red
      ;;
    esac
done

echo "";
coloredEcho "Ah, $persona! My favorite. Select your environment, $persona:" cyan

export persona=$(echo "$persona" | awk '{print tolower($0)}')

envs=("DDEV" "Valet/Herd")

select env in "${envs[@]}"; do
  case $env in
    "DDEV")
      coloredEcho "You chose DDEV" magenta
      bash -c "$(curl -s https://bitbucket.org/vaersaagod/vaersagod-bootstrap-2018-craft-3/raw/master/ddev.sh)"
      break
      ;;
    "Valet/Herd")
      coloredEcho "You chose VALET/HERD" cyan
      bash -c "$(curl -s https://bitbucket.org/vaersaagod/vaersagod-bootstrap-2018-craft-3/raw/master/valet.sh)"
      break
      ;;
    *) coloredEcho "Invalid option. Pick one or the other!" red
      ;;
    esac
done

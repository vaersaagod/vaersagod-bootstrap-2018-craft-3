#!/bin/bash

function coloredEcho {
    local exp=$1;
    local color=$2;
    local mod=$3;
    if ! [[ $color =~ '^[0-9]$' ]] ; then
       case $(echo $color | tr '[:upper:]' '[:lower:]') in
        black) color=0 ;;
        red) color=1 ;;
        green) color=2 ;;
        yellow) color=3 ;;
        blue) color=4 ;;
        magenta) color=5 ;;
        cyan) color=6 ;;
        white|*) color=7 ;; # white or invalid color
       esac
    fi
    tput setaf $color;
    echo $mod "$exp";
    tput sgr0;
}

function program_is_installed {
  local val=1
  type $1 >/dev/null 2>&1 || { local val=0; }
  echo "$val"
}

####### DETECT NEEDED SOFTWARE #######
SOFTWARE_OK=1

if [[ $(program_is_installed ddev) == 1 ]]; then
  DDEV=$(which ddev)
else
  coloredEcho "✗ You need DDEV installed on your system." red
  coloredEcho "See https://ddev.readthedocs.io/en/latest/users/install/ddev-installation/" red
  echo ''
  SOFTWARE_OK=0
fi

if [[ $SOFTWARE_OK == 0 ]]; then
  exit
fi

####### USER INPUT #######
echo "";
coloredEcho "Project directory (or ENTER for current): " cyan
read projectDir

####### HANDLE DIRECTORY INPUT #######
if [[ -z $projectDir ]]; then
  coloredEcho "Using current directory..." green
  echo ''
else
  projectDirSafe=${projectDir//[^a-zA-Z0-9.]/}
  if [[ -d "$projectDirSafe" ]]; then
    coloredEcho "✗ The directory '$projectDirSafe' already exits. Overwrite it? [y/N]: " red
    read projectDirOverwrite
    if [[ $projectDirOverwrite =~ ^[Yy]$ ]]; then
      rm -rf $projectDirSafe
    else
      coloredEcho "✗ Okay then, you figure it out..." red
      exit
    fi
  fi
  coloredEcho "Creating directory '$projectDirSafe' and switching to it..." green
  mkdir -- "$projectDirSafe" && cd -- "$projectDirSafe"
  echo ''
fi

coloredEcho "Project handle (or ENTER to use directory name): " cyan
read projectName
if [[ -z $projectName ]]; then
  projectName="${PWD##*/}"
fi
coloredEcho "Using project handle '$projectName'... " green
echo ''

coloredEcho "Project description (will be used for package.json): " cyan
read projectDesc

####### CREATE DDEV SITE #######
coloredEcho "Creating DDEV site https://$projectName.ddev.site..." cyan
ddev config --project-type=craftcms --docroot=web --create-docroot --project-name=$projectName --php-version=8.2 --database=mysql:8.0 --nodejs-version=18 --disable-settings-management --performance-mode=none
coloredEcho "✔ DONE" white

####### DOWNLOAD CRAFT STARTER PROJECT #######
echo ''
coloredEcho 'Downloading the Craft CMS starter project...' cyan
ddev composer create -y --no-scripts craftcms/craft:^4

# Delete all the things we don't need
ddev exec rm -f .env.example.dev
ddev exec rm -f .env.example.production
ddev exec rm -f .env.example.staging
ddev exec rm -rf config
ddev exec rm -rf templates
ddev exec rm -rf web
ddev exec rm -rf modules

####### DOWNLOAD VÆRSÅGOD BOILERPLATE #######
echo ''
coloredEcho 'Downloading the Værsågod boilerplate...' pink
mkdir -p tmp
git clone --depth=1 --quiet git@bitbucket.org:vaersaagod/vaersagod-boilerplate-2018-craft-3.git tmp
rm -rf tmp/.git

sed -i~ "/^PRIMARY_SITE_URL=/s/=.*/=https:\/\/${projectName}.ddev.site/" ./tmp/.env-example;
sed -i~ "/^CRAFT_SECURITY_KEY=/s/=.*/=${CRAFT_SECURITY_KEY}/" ./tmp/.env-example;
sed -i~ "/^CRAFT_APP_ID=/s/=.*/=${CRAFT_APP_ID}/" ./tmp/.env-example;
sed -i~ "/^CRAFT_DB_SERVER=/s/=.*/=db/" ./tmp/.env-example;
sed -i~ "/^CRAFT_DB_USER=/s/=.*/=db/" ./tmp/.env-example;
sed -i~ "/^CRAFT_DB_PASSWORD=/s/=.*/=db/" ./tmp/.env-example;
sed -i~ "/^CRAFT_DB_DATABASE=/s/=.*/=db/" ./tmp/.env-example;
sed -i~ "/^CRAFT_DB_TABLE_PREFIX=/s/=.*/=craft/" ./tmp/.env-example;
sed -i~ "/^CRAFT_DB_PORT=/s/=.*/=3306/" ./tmp/.env-example;

echo "" >> ./tmp/.env-example;
echo "HI_MY_NAME_IS=${persona}" >> ./tmp/.env-example;

find ./tmp -type f \( -name "*.php" -o -name "*.json" -o -name "*.conf" -o -name "*.md" -o -name "*.js" -o -name "*.txt" -o -name "*.webmanifest" -o -name ".env-example" \) -exec sed -i '' "s/VRSG_PROJECT_NAME/${projectName}/g" {} \;
find ./tmp -type f \( -name "*.php" -o -name "*.json" -o -name "*.conf" -o -name "*.md" -o -name "*.js" -o -name "*.txt" -o -name "*.webmanifest" -o -name ".env-example" \) -exec sed -i '' "s/VRSG_PROJECT_DESC/${projectDesc}/g" {} \;

cp -r ./tmp/. ./
rm -rf tmp
mv .env-example .env
rm -f .env-example
rm -f .env-example~

# Rename the path to the module
mv ./modules/VRSG_PROJECT_NAME "./modules/${projectName}"

coloredEcho "✔ DONE" white
echo ''

# Restart DDEV to make sure all our file changes above are synced
ddev restart

# Add our pubkey to the container
ddev auth ssh

# Config composer.json
coloredEcho 'Configuring composer.json...' cyan
ddev composer config minimum-stability dev
ddev composer config prefer-stable true
ddev composer config preferred-install dist
ddev composer config platform.php "8.2"

####### INSTALL EXTRA COMPOSER DEPENDENCIES #######
coloredEcho "Installing additional Composer dependencies..." cyan

# Craft plugins
declare -a pluginPackages
declare -a pluginHandles

pluginPackages[0]="vaersaagod/toolmate:^1.0"
pluginPackages[1]="vaersaagod/seomate:^2.0"
pluginPackages[2]="craftcms/ckeditor:^3.0"
pluginPackages[3]="spacecatninja/imager-x:^4.0"
pluginPackages[4]="mmikkel/child-me:^1.0"
pluginPackages[5]="mmikkel/cp-field-inspect:^1.0"
pluginPackages[6]="mmikkel/cp-clearcache:^1.0"
pluginPackages[7]="vaersaagod/assetmate:^2.0"
pluginPackages[8]="vaersaagod/redirectmate:^1.0"

pluginHandles[0]="toolmate"
pluginHandles[1]="seomate"
pluginHandles[2]="ckeditor"
pluginHandles[3]="imager-x"
pluginHandles[4]="child-me"
pluginHandles[5]="cp-field-inspect"
pluginHandles[6]="cp-clearcache"
pluginHandles[7]="assetmate"
pluginHandles[8]="redirectmate"

composerInstallStr="";

for i in "${pluginPackages[@]}"
do
    composerInstallStr="$composerInstallStr $i";
done

# Add redis extension
composerInstallStr="$composerInstallStr yiisoft/yii2-redis";

ddev composer require $composerInstallStr --prefer-dist -W;

echo ''
coloredEcho "✔ Done with all the Composer stuff" white

####### INSTALL CRAFT #####
coloredEcho "Now for the exciting part. Let's install Craft!" cyan
echo ''
ddev craft setup/app-id
ddev craft setup/security-key
ddev craft install

coloredEcho "✔ DONE" white
echo ''

###### INSTALL CRAFT PLUGINS ######
coloredEcho "Installing Craft plugins..." cyan
for i in "${pluginHandles[@]}"
do
    ddev craft plugin/install $i
done

coloredEcho "✔ DONE" white
echo ''

##### DEFAULT CONTENT MIGRATION #####
coloredEcho "Run default migrations? [y/N]: " cyan
read runMigrations

if [[ $runMigrations =~ ^[Yy]$ ]]; then
    coloredEcho 'Running default migrations...' green
    ddev craft migrate/up --interactive=0
else
    ddev exec rm ./migrations/m200324_104915_add_defaults.php
fi

coloredEcho "✔ DONE" white
echo ''

#### CK EDITOR CONFIGS
coloredEcho "Copying CKEditor configs..." cyan
ddev exec rm -rf ./config/project/ckeditor/configs
ddev exec mv ./_ckeditor ./config/project/ckeditor/configs
ddev craft project-config/touch
ddev craft project-config/apply

coloredEcho "✔ DONE" white
echo ''

##### INSTALL FRONT END DEPENDENCIES
coloredEcho "Installing NPM dependencies with Yarn..." green
ddev yarn
ddev build
coloredEcho "✔ DONE" white

echo ''
ddev exec node -v > .nvmrc;

echo ''
coloredEcho "That's it! Your site is available at https://$projectName.ddev.site" white
coloredEcho 'Run `$ ddev launch` to launch it now' white

echo ''

coloredEcho 'To start the dev server, run `$ ddev dev`' cyan
coloredEcho 'To build production assets, run `$ ddev build`' cyan

echo ''
coloredEcho "Now make something awesome!" cyan
echo ''

#!/bin/bash

function coloredEcho {
    local exp=$1;
    local color=$2;
    local mod=$3;
    if ! [[ $color =~ '^[0-9]$' ]] ; then
       case $(echo $color | tr '[:upper:]' '[:lower:]') in
        black) color=0 ;;
        red) color=1 ;;
        green) color=2 ;;
        yellow) color=3 ;;
        blue) color=4 ;;
        magenta) color=5 ;;
        cyan) color=6 ;;
        white|*) color=7 ;; # white or invalid color
       esac
    fi
    tput setaf $color;
    echo $mod "$exp";
    tput sgr0;
}

function program_is_installed {
  local val=1
  type $1 >/dev/null 2>&1 || { local val=0; }
  echo "$val"
}

####### DETECT NEEDED SOFTWARE #######
SOFTWARE_OK=1

if [[ -f ~/.nvm/nvm.sh ]]; then
  . ~/.nvm/nvm.sh
else
  coloredEcho "✗ This script needs nvm installed on your system." red
  coloredEcho "See https://github.com/creationix/nvm#installation" red
  echo ''
  SOFTWARE_OK=0
fi

if [[ $(program_is_installed node) == 1 ]]; then
  NODE=$(which node)
else
  coloredEcho "✗ This script needs node installed on your system." red
  coloredEcho "See https://nodejs.org/en/download/" red
  echo ''
  SOFTWARE_OK=0
fi

if [[ $(program_is_installed mysql) == 1 ]]; then
  MYSQL=$(which mysql)
else
  coloredEcho "✗ You need MySQL installed on your system." red
  coloredEcho "See https://dev.mysql.com/downloads/mysql/" red
  echo ''
  SOFTWARE_OK=0
fi

if [[ $(program_is_installed yarn) == 1 ]]; then
  YARN=$(which yarn)
else
  coloredEcho "✗ You need Yarn installed on your system." red
  coloredEcho "See https://yarnpkg.com/en/docs/install" red
  echo ''
  SOFTWARE_OK=0
fi

if [[ $(program_is_installed composer) == 1 ]]; then
  COMPOSER=$(which composer)
else
  coloredEcho "✗ You need Composer installed on your system." red
  coloredEcho "See https://getcomposer.org/download/" red
  echo ''
  SOFTWARE_OK=0
fi

if [[ $SOFTWARE_OK == 0 ]]; then
  exit
fi

####### USER INPUT #######
coloredEcho "Project directory (or ENTER for current): " cyan -n
read projectDir

####### HANDLE DIRECTORY INPUT #######
if [[ -z $projectDir ]]; then
  coloredEcho "Using current directory..." green
else
  projectDirSafe=${projectDir//[^a-zA-Z0-9.]/}
  if [[ -d "$projectDirSafe" ]]; then
    coloredEcho "✗ The directory '$projectDirSafe' already exits. Overwrite it? [y/N]: " red -n
    read projectDirOverwrite
    if [[ $projectDirOverwrite =~ ^[Yy]$ ]]; then
      rm -rf $projectDirSafe
    else
      coloredEcho "✗ Okay then, you figure it out..." red
      exit
    fi
  fi
  coloredEcho "Creating directory '$projectDirSafe' switching to it..." green
  mkdir -- "$projectDirSafe" && cd -- "$projectDirSafe"
  echo ''
fi

coloredEcho "Project handle (will be used for local database AND *.test prefix): " cyan -n
read projectname

coloredEcho "Project description (will be used for package.json): " cyan -n
read projectDesc

coloredEcho "Download extra plugins? [y/N]: " cyan -n
read downloadPlugins

coloredEcho "Run default content migrations? [y/N]: " cyan -n
read runMigrations

coloredEcho "Download and use the latest LTS of Node? [y/N]: " cyan -n
read updateNode

####### CREATE DATABASE #######
echo ''
export MYSQL_PWD=root
coloredEcho "Creating database craftcms_$projectname..." green
$MYSQL -u root -e "CREATE DATABASE IF NOT EXISTS craftcms_$projectname";
coloredEcho "✔ DONE" white

####### DOWNLOAD CRAFT #######
echo ''
coloredEcho 'Downloading Craft 3 with Composer...' green

composer create-project -s RC craftcms/craft:^3 . --no-interaction

# Save the security key Craft generated for us
source .env
rm -f .env
rm -f .env.example
rm -rf config
rm -rf templates
rm -rf web
rm -rf modules

# Config composer.json
coloredEcho 'Configuring composer.json...' green
composer config minimum-stability dev
composer config prefer-stable true
composer config preferred-install dist
composer config platform.php "7.4.13"
composer update

echo ''

####### DOWNLOAD VÆRSÅGOD BOILERPLATE #######
echo ''
mkdir -p tmp
coloredEcho 'Downloading boilerplate...' green
git clone --depth=1 --quiet -b craft3 git@bitbucket.org:vaersaagod/vaersagod-boilerplate-2018-craft-3.git tmp
rm -rf tmp/.git

find ./tmp -type f \( -name "*.php" -o -name "*.json" -o -name "*.conf" -o -name "*.md" -o -name "*.js" -o -name "*.txt" -o -name "*.webmanifest" -o -name ".env-example" \) -exec sed -i '' "s/VRSG_PROJECT_NAME/${projectname}/g" {} \;
find ./tmp -type f \( -name "*.php" -o -name "*.json" -o -name "*.conf" -o -name "*.md" -o -name "*.js" -o -name "*.txt" -o -name "*.webmanifest" -o -name ".env-example" \) -exec sed -i '' "s/VRSG_PROJECT_DESC/${projectDesc}/g" {} \;
find ./tmp/.env-example -exec sed -i '' "s/SECURITY_KEY=/SECURITY_KEY=${SECURITY_KEY}/g" {} \;
find ./tmp/.env-example -exec sed -i '' "s/APP_ID=/APP_ID=${APP_ID}/g" {} \;
cp -r ./tmp/. ./
rm -rf tmp

# Rename the path to the business logic module
mv ./modules/VRSG_PROJECT_NAME "./modules/${projectname}"

# Lock the Yarn version
yarn set version 1

coloredEcho "✔ DONE" white
echo ''

####### NVM/NODE SETUP #######
if [[ $updateNode =~ ^[Yy]$ ]]; then
  echo ''
  lts=$(nvm version-remote --lts)
  coloredEcho "Downloading and installing the latest LTS of Node: $lts..." green
  echo $lts > .nvmrc
  nvm install
  nvm use

  coloredEcho "✔ DONE" white
  echo ''
else
  echo $(nvm current) > .nvmrc
fi

nvm use

####### CLEANING UP #######
mv .env-example .env
rm -f .env-example

####### SETTING PERMISSIONS #######
echo ''
coloredEcho 'Setting permission levels...' green
permLevel=774
chmod $permLevel config
chmod $permLevel storage
chmod $permLevel vendor
coloredEcho "✔ DONE" white
echo ''

####### CREATE VALET HOST IF AVAILABLE #######
createValetHost="n"
if [[ $(program_is_installed valet) == 1 ]]; then
  VALET=$(which valet)

  coloredEcho "You have Valet installed. Do you want to create a host for $projectname.test? [y/N]: " cyan -n
  read createValetHost

  if [[ $createValetHost =~ ^[Yy]$ ]]; then

    coloredEcho "Setting up host $projectname.test with Valet..." green;
    coloredEcho "Do you want to secure the $projectname.test host? [y/N]: " cyan -n
    read createSecureValetHost

    if [[ $createSecureValetHost =~ ^[Yy]$ ]]; then
        $VALET link $projectname && $VALET secure $projectname;
        echo '';
        coloredEcho "Secure host https://$projectname.test created with Valet. To remove it, type $ valet unlink. To unsecure the host, type $ valet unsecure" green
    else
        $VALET link $projectname;
        echo '';
        coloredEcho "Host http://$projectname.test created with Valet. To remove it, type $ valet unlink" green
    fi

  fi

else

    coloredEcho "Add MAMP unix socket to database config? [y/N]: " cyan -n
    read addMampUnixSocket

    if [[ $addMampUnixSocket =~ ^[Yy]$ ]]; then
        find ./.env -exec sed -i '' "s/DB_SOCKET=\"\"/DB_SOCKET=\"\/Applications\/MAMP\/tmp\/mysql\/mysql.sock\"/g" {} \;
    fi

fi

####### INSTALLING CRAFT ######
coloredEcho "Now for the exciting part. Let's install Craft!" green
echo ''
./$projectFolder/craft install

coloredEcho "✔ DONE" white
echo ''

###### DOWNLOAD/INSTALL PLUGINS ######
if [[ $downloadPlugins =~ ^[Yy]$ ]]; then
  coloredEcho 'Installing all plugins...' green
else
  coloredEcho 'Installing default plugins only...' green
fi

declare -a requiredPlugins
declare -a defaultPlugins

declare -a requiredPluginHandles
declare -a defaultPluginHandles

requiredPlugins[0]="vaersaagod/toolmate"
requiredPlugins[1]="vaersaagod/seomate"
requiredPlugins[2]="craftcms/redactor"
requiredPlugins[3]="spacecatninja/imager-x"
requiredPlugins[4]="mmikkel/child-me"
requiredPlugins[5]="mmikkel/cp-field-inspect"
requiredPlugins[6]="mmikkel/cp-clearcache"
requiredPlugins[7]="verbb/image-resizer"

requiredPluginHandles[0]="toolmate"
requiredPluginHandles[1]="seomate"
requiredPluginHandles[2]="redactor"
requiredPluginHandles[3]="imager-x"
requiredPluginHandles[4]="child-me"
requiredPluginHandles[5]="cp-field-inspect"
requiredPluginHandles[6]="cp-clearcache"
requiredPluginHandles[7]="image-resizer"

defaultPlugins[0]="craftcms/aws-s3"
defaultPlugins[1]="verbb/super-table"
defaultPlugins[2]="vaersaagod/matrixmate"
defaultPlugins[3]="vaersaagod/linkmate"
defaultPlugins[4]="vaersaagod/instructions"

defaultPluginHandles[0]="aws-s3"
defaultPluginHandles[1]="super-table"
defaultPluginHandles[2]="matrixmate"
defaultPluginHandles[3]="linkmate"
defaultPluginHandles[4]="instructions"

composerInstallStr="";

for i in "${requiredPlugins[@]}"
do
    composerInstallStr="$composerInstallStr $i";
done

if [[ $downloadPlugins =~ ^[Yy]$ ]]; then
    for i in "${defaultPlugins[@]}"
    do
        composerInstallStr="$composerInstallStr $i";
    done
fi

# Add redis extension
composerInstallStr="$composerInstallStr yiisoft/yii2-redis";

composer require $composerInstallStr --prefer-dist -W;

echo ''
coloredEcho "✔ Plugin composer install done" white

for i in "${requiredPluginHandles[@]}"
do
    ./craft plugin/install $i
done

if [[ $downloadPlugins =~ ^[Yy]$ ]]; then
    for i in "${defaultPluginHandles[@]}"
    do
        ./craft plugin/install $i
    done
fi

echo ''
coloredEcho "✔ Plugin Craft install done" white

if [[ $runMigrations =~ ^[Yy]$ ]]; then
    coloredEcho 'Running default migrations...' green
    ./craft migrate/up --interactive=0
    coloredEcho "✔ DONE" white
else
    rm ./migrations/m200324_104915_add_defaults.php
fi

echo ''
coloredEcho "Do you want to install front end dependencies with Yarn now? [y/N]: " cyan -n
read installYarnDependencies

if [[ $installYarnDependencies =~ ^[Yy]$ ]]; then
    coloredEcho 'Installing front end dependencies...' green
    yarn && yarn build
fi

coloredEcho "✔ DONE" white
echo ''

echo ''
coloredEcho "Now..." green
echo '';

if [[ $createValetHost =~ ^[Yy]$ ]]; then
    coloredEcho " * Check settings in the .env file" magenta
else
    coloredEcho " * Create a host for http://$projectname.test" magenta
    coloredEcho " * Check settings in the .env file" magenta
fi

if [[ $installYarnDependencies =~ ^[Nn]$ ]]; then
    coloredEcho " * Type $ yarn && yarn build" magenta
fi

coloredEcho " * Type $ yarn start" magenta

echo ''

coloredEcho "That's it. Now make something awesome!" white
echo ''
